#include<stdio.h>
#include<time.h>
#include<string.h>
#define BUFFER_SIZE 1024
void compareFiles(FILE *, FILE *);
void main(int argc,char *argv[])
{
    clock_t start = clock();
    double exeTime = 0.0;
    FILE *fptr1,*fptr2;
    
   if (argc < 3)
    {
        printf("File names missing");
        return;
    }
       
    if((fptr1 = fopen(argv[1],"r"))==NULL)
    {
        printf("Cannot open first file");
        return;
    }
    else
    {
        
        if((fptr2 = fopen(argv[2],"r"))==NULL)
        {
            printf("Cannot open second file");
            return;
        }
        else
        {            
            compareFiles(fptr1,fptr2);
        }
    }

    

    clock_t stop = clock();
    exeTime += (double)(stop - start) / CLOCKS_PER_SEC;
	printf("Time elapsed is %f seconds", exeTime);

}

void compareFiles(FILE *fptr1,FILE *fptr2)
{
    char temp1[BUFFER_SIZE],temp2[BUFFER_SIZE];
    int i,k,count=0,length1,length2;
    fseek(fptr1,0,SEEK_END);
    fseek(fptr2,0,SEEK_END);
    length1 = ftell(fptr1);
    length2 = ftell(fptr2);
    fseek(fptr1,0,SEEK_SET);
    fseek(fptr2,0,SEEK_SET);

    while(!feof(fptr1) && !feof(fptr2))
   {
       fgets(temp1,BUFFER_SIZE,fptr1);
       fgets(temp2,BUFFER_SIZE,fptr2);
       if(strcmp(temp1,temp2)==0)
        {
            count++;
            continue;            
        }
        
        for(i=0;i<BUFFER_SIZE;i++)
        {
            if(temp1[i]==temp2[i])
            {
                continue;
            }
            break;
        }
        
        printf("offset %d\n",i+1+(count*BUFFER_SIZE));
        k=i;
        printf("Difference from first file:");
        for(int j=0;j<16 && k<BUFFER_SIZE && k < length1;j++)
        {
            printf("%x",temp1[k]);
            k++;
        }
        k=i;
        printf("\nDifference from second file:");
        for(int j=0;j<16 && k<BUFFER_SIZE && k < length2;j++)
        {
            printf("%x",temp2[k]);
            k++;
        }
        printf("\n");      
        return;
       
   }
    
   printf("Both the files are equal\n");
}
